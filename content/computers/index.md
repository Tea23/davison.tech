---
title: "Computers"
draft: false
menu:
  main:
    weight: 20
---

Documentation of my various devices. I name them, primarily, after places in Alberta. PC Part Picker links are included.

## Jasper

My main gaming / general stuff-doing PC. Built in 2020, rather shamefully, as pandemic boredom relief, originally with a 1070 that I bought back in 2016 in the UK.
It runs Arch Linux, on which most games work perfectly fine through Valve's Proton.

Type|Item
:----|:----
**CPU** | [AMD Ryzen 7 5700X 3.4 GHz 8-Core Processor](https://ca.pcpartpicker.com/product/JmhFf7/amd-ryzen-7-5700x-34-ghz-8-core-processor-100-100000926wof)
**CPU Cooler** | [Cooler Master MASTERLIQUID ML240L RGB V2 65.59 CFM Liquid CPU Cooler](https://ca.pcpartpicker.com/product/fLFKHx/cooler-master-masterliquid-ml240l-rgb-v2-6559-cfm-liquid-cpu-cooler-mlw-d24m-a18pc-r2)
**Motherboard** | [Asus PRIME B550M-A/CSM Micro ATX AM4 Motherboard](https://ca.pcpartpicker.com/product/VpLwrH/asus-prime-b550m-acsm-micro-atx-am4-motherboard-prime-b550m-acsm)
**Memory** | [G.Skill Value 16 GB (2 x 8 GB) DDR4-2666 CL19 Memory](https://ca.pcpartpicker.com/product/vzkj4D/gskill-value-16gb-2-x-8gb-ddr4-2666-memory-f4-2666c19d-16gnt)
**Memory** | [G.Skill Value 16 GB (2 x 8 GB) DDR4-2666 CL19 Memory](https://ca.pcpartpicker.com/product/vzkj4D/gskill-value-16gb-2-x-8gb-ddr4-2666-memory-f4-2666c19d-16gnt)
**Storage** | [Samsung 860 Evo 500 GB 2.5" Solid State Drive](https://ca.pcpartpicker.com/product/6yKcCJ/samsung-860-evo-500gb-25-solid-state-drive-mz-76e500bam)
**Storage** | [Samsung 860 Evo 500 GB 2.5" Solid State Drive](https://ca.pcpartpicker.com/product/6yKcCJ/samsung-860-evo-500gb-25-solid-state-drive-mz-76e500bam)
**Storage** | [Samsung 870 Evo 500 GB 2.5" Solid State Drive](https://ca.pcpartpicker.com/product/mRpmP6/samsung-870-evo-500-gb-25-solid-state-drive-mz-77e500bam)
**Storage** | [Kingston NV2 1 TB M.2-2280 PCIe 4.0 X4 NVME Solid State Drive](https://ca.pcpartpicker.com/product/FnYmP6/kingston-nv2-1-tb-m2-2280-pcie-40-x4-nvme-solid-state-drive-snv2s1000g)
**Video Card** | [ASRock Radeon RX6700XT CLD 12G Radeon RX 6700 XT 12 GB Video Card](https://ca.pcpartpicker.com/product/8HBG3C/asrock-radeon-rx-6700-xt-12-gb-challenger-d-video-card-rx6700xt-cld-12g)
**Case** | [Fractal Design Define Mini C MicroATX Mid Tower Case](https://ca.pcpartpicker.com/product/vBTrxr/fractal-design-define-mini-c-microatx-mid-tower-case-fd-ca-def-mini-c-bk)
**Power Supply** | [EVGA 600 BQ 600 W 80+ Bronze Certified Semi-modular ATX Power Supply](https://ca.pcpartpicker.com/product/DmWrxr/evga-600-bq-600-w-80-bronze-certified-semi-modular-atx-power-supply-110-bq-0600-k1)
**OS** | Arch Linux

## Canmore

A Thinkpad T420 that I've had since University. Canmore's recently been upgraded to an SSD and it continues to serve me very well, but some
of its shortcomings became apparent when I was OOMing during Zoom calls in my last job. Not fun!

It doesn't get used very often anymore, but one of the cats likes using it as a seat.

## Banff

Banff is a [Hetzner VPS](https://hetzner.cloud/?ref=hSRR0VT3LVgl) that this website runs on and is the central powerhouse for my personal digital infrastructure.

## Portsmouth

Portsmouth is also a [Hetzner VPS](https://hetzner.cloud/?ref=hSRR0VT3LVgl). One of my ongoing obligations
is to my Scout Group in the UK, the [8th Portsmouth](https://8thportsmouth.org.uk). I host their website and provide mail forwarding for them;
this all happens on Portsmouth. It breaks the Alberta naming rule for obvious reasons.

## Drumheller

Drumheller is a Gigabyte Intel NUC that is serving as my home server. It's set up to run Jellyfin and Pihole among other stuff. It's not
pretty, it's barely *safe*, but it's been a fun thing to play around with. Some day I will have a proper home server.

Recently, I migrated it from Arch to Debian. Something about that seems sensible.

## Vulcan

A woefully underutilised Raspberry Pi 4 8GB. They went on sale at Memory Express around Christmas time, so I nabbed one. At the moment,
it only runs LibreELEC and is our main Netflix client. I should do something else with it though since it's such a beefy little thing. Vulcan
seemed an appropriate name since we watch an unhealthy amount of Star Trek in our house - and it IS a place in Alberta.
