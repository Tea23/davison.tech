---
title: "I don't host my own email anymore"
date: 2023-02-27T13:56:00-07:00
draft: false
---

One thing that has built up over the years has been my many domain names. I started out with domain.com as I was a puerile teenager (although, I realise, I could have picked something MUCH worse!). I also have never wanted to use gmail or big email providers, and so I've hosted my own email since around 2009.

Recently, I migrated my server away from Ubuntu to Debian and used the opportunity to seek out a cheaper server provider (I ended up with Hetzner) and during the migration I was suddenly hit with the reality of setting up a mail server again.

And I just didn't want to.

I thought of the many undelivered emails. The spam. The day a catastrophic failure knocked everything offline and I'd lose a decade+ of emails...

Even with Amazon SES I was getting delivery failures.

So those days are over, and I moved everything to Fastmail. I could have gone with Proton, but Fastmail seems to offer the best balance on price. $5/month. For email. Reduce that a bit by prepaying for a few years - that's fine. I'll do that.

So yes, my email data is in the hands of Yet Another Company, and maybe I might still have delivery issues here and there. But I'm not the responsible party anymore, and it's a huge load off my mind.

I've been very happy with the migration so far - the built in tools to log into the old server and grab all the email was just fine, and I was even able to utilise the opportunity to consolidate my Too Many Fucking Email Addresses into one account, and I can slowly start killing off the others.

Even with standards like SPF, it does seem like the era of self-hosted email might be over. I lament this, and I hope we overcome it some day. But I'm glad to have finally cleaned some stuff up - it was long overdue.