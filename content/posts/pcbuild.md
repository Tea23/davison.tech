---
title: "New PC Build"
date: 2021-03-26T21:39:09-06:00
draft: false
---

Before my last full-time contract ended unexpectedly early, I put together the pieces for an office PC to work from home on.
I needed something separate from my gaming PC that could live permanently in my home office and be ready to go for any work-from-home
jobs I ended up taking; of course, we're all going to be working from home for the foreseeable, so why not be prepared?

Here's what I settled on, exported from PC Part Picker:

Type|Item|Price
:----|:----|:----
**CPU** | [AMD Ryzen 5 3400G 3.7 GHz Quad-Core Processor](https://pcpartpicker.com/product/XP6qqs/amd-ryzen-5-3400g-37-ghz-quad-core-processor-ryzen-5-3400g) | Purchased For $209.99 
**Motherboard** | [ASRock B450M-HDV R4.0 Micro ATX AM4 Motherboard](https://pcpartpicker.com/product/RD97YJ/asrock-b450m-hdv-r40-micro-atx-am4-motherboard-b450m-hdv-r40) | Purchased For $88.62 
**Memory** | [G.Skill Aegis 16 GB (2 x 8 GB) DDR4-3000 CL16 Memory](https://pcpartpicker.com/product/FNprxr/gskill-aegis-16gb-2-x-8gb-ddr4-3000-memory-f43000c16d16gisb) | Purchased For $85.03 
**Storage** | [Kingston A400 240 GB 2.5" Solid State Drive](https://pcpartpicker.com/product/btDzK8/kingston-a400-240gb-25-solid-state-drive-sa400s37240g) | Purchased For $33.99 
**Case** | [Antec VSK3000E U3 MicroATX Mini Tower Case](https://pcpartpicker.com/product/dH448d/antec-vsk3000e-u3-microatx-mini-tower-case-vsk3000e-u3) | Purchased For $39.75 
**Power Supply** | [EVGA BR 500 W 80+ Bronze Certified ATX Power Supply](https://pcpartpicker.com/product/kCtQzy/evga-br-500w-80-bronze-certified-atx-power-supply-100-br-0500-k1) | Purchased For $70.75 
**OS**| Fedora Workstation|  

The motherboard, the B450M-HDV R4.0, has served me well in my gaming PC for the past year when I built it to give me something to do
over the first few pandemic months. It's a no-frills board with just a little room for expandability. Being fairly inexpensive, it was
never going to do anything fancy, but it doesn't need to. This needed to be paired with an AM4 CPU with on-chip graphics and the 3400G
was an obvious choice for striking a decent balance between performance and price. Unfortunately, given the global chip shortages, it took
a fair while to get my hands on one and by the time it arrived my contract had been ended. Not to worry though, since everything is ready
for my next gig!

RAM, storage, PSU, and case choices were all made in consideration of cost. I couldn't break the bank with this build. The RAM and the SSD
are nothing to write home about; it tends to be the case in the modern market that paying more for RAM might get you faster clocks or a fancy,
yet superfluous, heatsink. Kingston is an excellent brand for SSDs, and I make it a rule to only go with them or Samsung, depending on
what my needs are. There's nothing particularly special about the A400, and I didn't need much space.

My regrets stem from the case and PSU, however. My gaming PC was built with a Masterbox Q300L and a semi-modular PSU. Not dealing
with spidery cable spaghetti was a blessing and the case, though unconventional in its design, is perfectly serviceable and has great cable
management.

But not this case. No. Ugh. The VSK3000E from Antec is budget with a capital B. It has razor sharp edges adding a sense of danger to 
the build process and a top-mount PSU bay (which, in retrospect, I don't mind too much, since it allows me to set the PC directly on the carpet). 
Its cable management is...interesting. There are cut outs for cables to travel along, but I actually wonder if they're manufacturing errors, since
there is actually no way to put the side panel back on if you actually feed your cables back there. So you're left with having to tuck
cables into the optical drive bay and ziptyping it as much as possible. The PSU not being at all modular makes this even more challenging.

But I got what I expected, for the most part. I'm terribly disappointed in the case and won't build with one of them again, but for this,
a PC that needs to be ready to go and won't necessarily be upgraded at all in the future, it's fine. The build is fine. Performance is
admirable, too. I want to use Linux for the main desktop, but any business I end up working for in the future is going to have *some*
requirement on MS Office, so a virtual machine is a necessity. For this reason I may add a simple dedicated GPU to allow for the VM
to be graphically accelerated, but in the meantime, there's some choppy window movement in the Windows guest, but it's not so bad.

On Linux, the choice of Fedora was largely pragmatic. I'd typically prefer to run Arch and I have a great deal of fun running Arch on my
gaming PC and most other systems. But work isn't fun, so the distro choice needs to strike that balance between flexibility and stability. I 
believe in Arch as a platform and can work around breakages for the most part (also Arch doesn't break itself!), but I don't want to spending
time on the clock doing that. Fedora has a great desktop package and sometimes having stuff configured for you is pretty great, especially
if you have that option to break the mold later if you need to.

Overall, very happy with the build. I'm glad I put the time into it; it's just a shame I didn't get to do enough work on it to make it a tax
write-off!
