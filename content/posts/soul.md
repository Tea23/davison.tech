---
title: "Selling my Soul"
date: 2023-02-16T09:54:37-07:00
draft: false
---

A curious thing has happened in the last year or so: I got fat.

Working from home did a real number on my body; I found very few reasons to 
go out on walks or to exercise and the fears of the pandemic made even wanting 
to go to a bar with some friends seem like a mammoth task.

I don't know if I was fully agoraphobic or anything, but it wasn't great.

And so I've done what the 70-75kg version of me would consider unthinkable: I've bought a fitbit.

At a healthy weight, in a healthy economic situation, with a decent social life, the siren call of giving up a little bit of your data in exchange for something 
seems unconvincing. I avoided Facebook, I'd barely use Google Maps or location services. I wouldn't even fire up something like Shazam, no matter how much 
the beats on the PA appealed to me.

But ~25kg on, I'm happily using the fitbit to give me stats on how I'm doing when I bike or walk to work, as well as my progress throughout the day.

This has given me pause for thought: are data sapping companies like Google and Facebook inherently exploitative of those of us who are unhealthy?

The answer is probably a resounding yes, but we see the rich and famous with their Apple Watches all the time.

Society is *overwhelmingly* weighted against the underprivileged, but for a long time I always figured that just referred to the *economically* underprivileged. I'm not *poor* - I have enough money to pay my rent and feed myself, my partner, and my menagerie. I can even grab a few video games here and there. But I'm fat. I have an unhealthy weight. I struggle walking uphill, my breathing is heavy, and I'm sleeping like shit.

And as a result, I'm freely giving up - and in fact, paying for the privilege - *biometric data* to some company. Storing it all in the ~\*~\*~cloud~\*~\*~. ~25kg ago, I'd have found that egregious. I campaigned against it. I'd tell my friends not to give in to anything like that. I ate dinner with Richard Stallman[^1] and I talked about how it's fascinating and terrifying that we are making a commodity of data and that people are finding it glorious. I didn't actually really figure it was - potentially - an economic issue.

OR perhaps the case is simply that I'm approaching 30 and as I get older, I just care less. Can't beat 'em, join 'em?

[Merry Hell's Rage Like Thunder](https://www.youtube.com/watch?v=307cSF789lM) springs to mind.

[^1]: Just had to drop that in there.