---
title: "I lost my 9 year old home directory"
date: 2022-09-18T13:55:10-06:00
draft: false
---

I don't know why, but when someone told me "if it ain't broke, don't fix it", I completely ignored them.

Back in 2013 I built the first PC that I could call "my own". And, naturally, it ran Arch. The home directory
on this PC has travelled across the world with me, living on several physical disks and ultimately always being
the basis of my Linux installs. It contained settings and preferences for nearly a decade of applications,
desktop environments, web browsers. It has save games. Some notes from university. A cache of immmigration 
documents. A (thankfully stored elsewhere) music collection. Many photos of my pets.

And now it's all gone. Why? Hubris, I guess.

There's nothing wrong with ext4, but for some reason I wanted to try btrfs. Converting my games disks and even my 
root partition from ext4 to btrfs worked fine and I figured there can be no possible reason it would fail for my home FS either.

But it did. I kept running into a weird error pertaining to an operation not being permitted. I couldn't
find any solid information on this, except for one StackOverflow answer that implied that such a problem
can be caused by attempting the conversion on a filesystem that was once LUKS encrypted. My home dir
was at one point (re-evaluating my threat model I decided an encrypted home directory on a desktop that
never leaves my house was an unnecessary inconvenience - I kept forgetting my passphrase).

I proceeded to explore restoring bad superblocks to see if that would do it, and this lead me down the path
of trying `mke2fs -S`, which turned out to be a fatal mistake.

`-S` rebuilds all superblocks and apparently you should only run this "if you feel lucky". Well, I did feel lucky.

The result was that I now had an empty ext4 filesystem where once my home directory sat.

I tried exploring a few recovery options and while I'm sure I didn't exhaust all possibilities, and it is indeed probably
quite possible that my data could have been recovered... I just gave up.

And I am okay with that.

9 years is a long time. I'm a different person today from who I was then. Is it perhaps possible that carrying around 9 years
of dotfiles and cruft was holding me back?

Maybe.

I don't really know. I just know that I have an empty home directory now and... I think it's okay. I'm working on rebuilding it and
this process isn't easy. I will frequently run into something that doesn't work like it used to anymore and upon asking why, the answer
is most likely that I had a dotfile that handled that for me.

*One unexpected* casuality was one of my games disks. In an act of desperation I attempted to do `ddrescue` of the disk and needed an equivalent
disk to copy the 'rescued' data over to. Both the home and games disks are the same make, model, and capacity so I went for it. I gotta
download all those games again now. Not a big deal.

I haven't lost anything of value (*I think*). Most important documents and files are stored elsewhere and there is a hard drive on my desk which
*may* contain an older version of the directory; we can explore that and see.

The process of configuring everything again is annoying - but it's no big deal.

Moral of the story? I dunno. Don't fix what ain't broke? Or something?
