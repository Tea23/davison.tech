---
title: "Watching IRC die"
date: 2022-11-28T23:57:07-07:00
draft: true
---

I think I'm ready to admit that IRC is dying. At least for me. I've been hanging out in IRC channels 
for 15 years, passively and actively. I was even instrumental in the technical and political 
administration of ScoutLink for several years.

But I've been looking at my ever-shrinking list of channels that I'm connected to in my bouncer and I'm 
starting to have an existential crisis about it at all. I can't even be bothered maintaining that bouncer.

The list is shrinking *rapidly* and I don't really know what to do about it.

I don't like Discord as a 'replacement' for IRC. Tying a chat protocol to a single client doesn't sit
well to me. I've found it impossible to "keep up" with everything in Discord servers - I'll find a project
that I like, look at its 'server' for a little bit and then just forget about it.

I'm not sure where this problem comes from. Realistically, a heap of Discord servers and a throng of 
IRC channels really aren't all too disimilar. So what's the problem? Is it the UX of the client? I'm not sure, 
because times when I've tied Discord into bitlbee (thus making the UX virtually identical to IRC), I still just... don't care.

So there's surely *something* about IRC in particular that keeps my attention, but it's becoming quieter day by day.

I used to be active in most of the channels on my list - `#archlinux` among them, but it just isn't the case anymore.

It's a weird time, and I don't really understand it. Maybe I'm just wistful because of a recent relationship breakdown (that also was very much tied 
to IRC).

All this, and I just really don't want to admit that centralised services are beating us nerds. Fuck, all those proprietary Electron clients, man.
