---
title: "Webhooks"
date: 2021-03-28T19:30:24-06:00
draft: false
---

When I set up my new (well now new**ish**) server, I gave myself the challenge of running as much as possible
in docker containers. I wasn't able to satisfactorily run Hugo itself in a container, but I don't mind much
since it's a static site generator and I think Docker would be largely pointless. I wanted to try and get
webhooks working, also, but wasn't able to in Docker either.

I am certain there's more thinking and research I need to do; it must be possible to manage webhooks in a docker
container, but since we rely on running `git` and `hugo` to build the site, I can't think of a way to do it short
of creating a new image. I *could* do that, but I just feel it would be needless complexity for something of this
scale. At the very least, I've figured out the basics of webhooks and that's fine, and as a result I can push
directly to my repository on GitLab and instantly have posts and content updates here be published, without needing
to log in to my server.

I've achieved this using [adnanh's webhook server](https://github.com/adnanh/webhook) and I keep it running on
the host, managed with supervisor. It's not a great solution when you're trying to keep things in containers, but
it's fine. My script for pulling and rebuilding the site is also highly rudimentary; just two lines: `git pull` and `hugo -D`.
Not the best, and contains no failsafes, but it's fine for now. I also don't *love* that the webhook server is exposed
on the host, but some access rules could alleviate any possible grief from this, and since GitLab will send requests
over HTTPS with a secret token, it's not so bad.

All in all, I'm happy with the new set up. It's taught me what I need to know about webhooks and I've devised a way
to author a website statelessly. Pretty neat.
