---
title: "What I learned from my Docker experiment"
date: 2022-09-16T23:26:52-06:00
draft: false
---

Something seems to happen when I change jobs - I look at my personal IT infrastructure and I start screwing around with stuff.

I'm about to move into the public sector after being with a small company for a while. I had a great time there, but it's really time to stretch out.

In the past year I've dipped my toes into the world of SaaS sysadmin, become a veritable *expert* in SugarCRM, and even become a half-decent IT admin
for an SME. It's been a good time. I'm moving on to a world where I can be more of a generalist now, though, and play with technologies and just *things*
from everywhere.

I'm not sure why all of that has added up to me deciding to forego Docker on my personal server, but it somehow just...did. So anyway, over the past few days
I've been migrating data out of their containers and putting all of the services I can move out of Docker back into the host OS and I think I like it this way better.

Some things have had to stay in Docker - I've come to rely quite heavily on my [Mailcow](https://mailcow.email) install and I just don't want to go through the pain and misery
of moving everything out into the host OS, because email is such a pain in the ass anyway. Another thing staying in Docker is [Vaultwarden](https://github.com/dani-garcia/vaultwarden),
again primarily because moving data out might be a bit tricky and because it's sensitive data I'd rather not play with fire. Plus Vaultwarden is just such a good
implementation of Bitwarden and I'm not too sure there's a better alternative out there.

I've just really come to realise that running a web server out of Docker, or really any services that can just be installed on the host OS, is kinda pointless. It makes
data retrieval a bit harder, it clutters up the output of simple commands like `mount` and `ip -a`, and ultimately it just *isn't necessary* for a small, hobbyist scale.

Even a professional scale - while deploying a SaaS app with my old company, I found very little reason to use Docker for it.

So - I am barely using Docker anymore. And I'm okay with that. It was a fun experiment while it lasted.

Will I regret this later? Maybe. I can imagine that migrating servers just become much more difficult (should such a day come). But fuck it. My personal IT infrastructure
has never been for anything other than just *fun*.
