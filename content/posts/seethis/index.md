---
title: "See this? WSDD is good for Samba"
date: 2023-02-27T18:30:11-07:00
draft: false
---

DO YOU SEE THIS?!

![DRUMHELLER, a Linux Samba share, is visible in the Windows 'Network' locations interface.](images/seethis.png)

For far, far too long I have tried to have Samba shares be 'discoverable' to Windows clients on my home network. Turns out there's a thing for this now!

[wsdd](https://github.com/christgau/wsdd) is an astounding thing that tacks on Web Service Discovery to Samba, making Samba shares on Linux hosts play nicely with Windows clients. Turns out, this is necessary because Samba only provides 'discoverability' through NETBIOS/SMB1, and the only way - previously - to get discoverable shares was to turn on insecure technology, which isn't nice.

I'm running Debian bookworm on my NUC right now, and `wsdd` is a package in bookworm. So a simple `sudo apt install wsdd` and the Linux share is just suddenly available on my home network as if it was a Windows share.

On other distributions, or older versions of Debian, things are probably weirder. Arch at least has it in the [AUR](https://aur.archlinux.org/packages/wsdd) (obviously). Ubuntu also has it upstreamed as of Jammy.

FINALLY.