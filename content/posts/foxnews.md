---
title: "The Fox News 'official website' was wrong on Wikipedia for 2 months"
date: 2023-04-18T16:17:03-06:00
draft: false
---

It came up in conversation recently about how Fox News gets away with so much because, officially, they are an "entertainment network" instead of a news network. During a moment of boredom at work I found myself looking at their Wikipedia article to see if that's a widely documented fact (it sort of is).

I was drawn to the infobox and the website field. It was a `t.me` link. Weird. The link went to a Telegram account that had 5 subscribers and no official checkmark.

Obviously when we see something incorrect on Wikipedia we can go and change it, but normally you find that high traffic articles get edited FAST. Try adjusting the History section of the article - add in a bit about how Rupert Murdoch launched the network to fund his need for a new ice cream maker (don't actually do this). It'll get reverted either by a bot or a hawkeyed editor. Massive props to the dedicated volunteers who keep Wikipedia relevant and accurate.

So I figured it must have been a very recent edit. I refreshed a couple of times - normally when I visit an article with something incorrect on it, refreshing a few times shows me that it's already been corrected. But it stayed the same. I checked the edit history on the article - 24 hours prior! This obviously fake link had survived on the Wikipedia article for well over a day! That's already pretty unusual.

I decided to log in and try to change it, but I see that the website is being populated by a template - `website = {official URL}`. I'm not a skilled or active Wikipedian so were it not for the little pencil icon in the infobox itself I'd have given up at this point. Thankfully though, clicking that takes you straight to the Wiki*data* entry for Fox News and down to the Official Website section.

Lo and behold there it is - an entry pointing to the Telegram channel. I struggled a little with the UI but eventually got it fixed.

How long had this blatant lie been there? Let's look at the history...

`23:59, 10 February 2023 95.55.10.236 talk 75,902 bytes +645 Created claim: official website (P856): <snip> undo Tag: Wikidata User Interface (restore)`

2 months! 10th Februrary 2023. And where's the IP located? Russia, according to a WHOIS query anyway.

I don't really know what happened here - the IP looks residential so it was probably just innocent vandalism. But what was the Telegram link all about? Where did it go? Who did it bamboozle? And more importantly - why did this get missed for so long?