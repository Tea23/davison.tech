---
title: "Brexit"
draft: false
---

"Brexit refugee" is, obviously, a jocular term. I have very little opinion regarding the actual machinations
of Britain leaving the EU, my issue will always be with the nastiness that came out of the woodwork
as a result of the referendum.

I don't have very many positive memories of the UK post-2016. And sure, Alberta might have its... *quirks*,
but there's more room (physically and metaphorically) in Canada to grow.

Anyway, that's as political as this website gets. And that's pretty funny, considering I'm an International
Relations graduate.

[Go back to the about page.](/about/)
