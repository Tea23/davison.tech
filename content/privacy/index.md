---
title: "Privacy"
date: 2023-03-28T10:27:44-06:00
draft: false
---

I use the privacy-respecting <a href="https://matomo.org">Matomo</a> tracker on this website to gather metrics on interest and usage. This data is purely for my own analysis and I never share it with anyone, either privately or commercially. As of March 28th, 2023, I have not received any requests from any government or commercial entity, law-enforcement agency, or any other party to share any of my tracking data.

My main purpose for doing this is, as this is a personal blog that I deliberately construct so as to be of interest to potential employers, is to get a little heads up that someone might be interested in my work - similar to getting a LinkedIn profile view notification.

Matomo records your browser type and version, approximate location, IP address, operating system, screen resolution, and number of visits. It assigns a unique ID number to each visitor based on these data points. Data points are subject to manipulation by your browser (ie, I respect user agent switchers, proxies/VPNs, TOR, etc).

I urge you to read more about Matomo's tracking techniques on their website, and to study its source code.

I am more than happy to indulge requests to see data that I have collected. To process these, send me an email at joe@davison.tech. Verification of your identity will, by necessity, have to take place on a case-by-case basis.

Matomo respects your browser's "Do Not Track" setting and I do not see any visits logged from browsers that have DNT enabled. There is also a cookie-based opt-out below:

<div>
<div id="matomo-opt-out"></div>
<script src="https://davison.tech/matomo/index.php?module=CoreAdminHome&action=optOutJS&divId=matomo-opt-out&language=auto&showIntro=1"></script>
</div>