---
title: "About"
draft: false
---

I'm Joe, a [Brexit refugee](/brexit/) living in Calgary, AB. I came to Canada from the United Kingdom in April 2019 to get a fresh start to get a foothold in the IT industry by graduating my research skills and inquisitive nature to a professional setting.

I blog infrequently here, largely on a self-referential basis, about things I've learned and accomplished in the Linux and systems administration space.

I ate Chinese food with Richard Stallman once.

I can be reached at **joe@davison.tech**.
